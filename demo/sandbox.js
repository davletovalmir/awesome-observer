class Test {
	constructor (v) {
  	this.v = v;
  }

  get v2 () {
  	return this.v * 2;
  }

  v3 (v) {
  	this.v4 = v;
  }
};

let state = {
	counter: 0,
  arrayWithObjectInside: [
		{some: 0, values: [1, 2, 3]},
		{some: 1, values: [4, 5, 6]},
		{some: 2, values: [7, 8, 9]}
	],
  testInstance: new Test(10),
  object: {
  	number: 10,
		string: "this is string",
		array: [1, 2, 3, 4, 5],
		object: {
			laugh: 'ahaha'
		}
  }
};

state = AwesomeObserver.observe(state);
window.state = state;

state.subscribe({
	on: 'change',
	only: ['arrayWithObjectInside'],
	do: ({target, key, value, prevValue, root, parent}) => {
		console.log(`change only arrayWithObjectInside, key: ${key}, value: ${value}, prev: ${prevValue}`);
	}
});

state.object.subscribe({
	on: 'change',
	do: ({currentTarget, target, key, value, prevValue, root, parent}) => {
		console.log(`change anything, key: ${key}, value: ${value}, prev: ${prevValue}`);
		console.log("target", Object.keys(target));
		console.log("currentTarget", Object.keys(currentTarget));
	}
});

state.subscribe({
	on: 'new',
	do: ({target, key, value, root, parent}) => {
		console.log(`new anything, key: ${key}, value: ${value}`);
	}
});

state.subscribe({
	on: 'delete',
	do: ({target, key, prevValue, root, parent}) => {
		console.log(`delete anything, key: ${key}, prevValue: ${prevValue}`);
	}
});

state.subscribe({
	on: 'get',
	only: ['counter'],
	do: ({target, key, value, root, parent}) => {
		console.log(`get anything, key: ${key}, value: ${value}`);
	}
});

state.object.subscribe({
	on: 'change',
	do: ({target, key, value, prevValue, root, parent}) => {
		console.log(`change state.object, key: ${key}, value: ${value}, prev: ${prevValue}`);
	}
});
