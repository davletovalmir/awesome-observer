var gulp       = require('gulp'),
    watch      = require('gulp-watch'),
    concat     = require('gulp-concat'),
    uglify     = require('gulp-uglify'),
    rename     = require('gulp-rename'),
    moment     = require('moment'),
    notify     = require('gulp-notify'),
    browserify = require('gulp-browserify');

gulp.task('uglify-js', function() {
    gulp.src(['src/index.js'])
        .pipe(browserify({insertGlobals: true}))
        .on('error', notify.onError("Error: <%= error.message %>"))
        .pipe(uglify())
        .on('error', notify.onError("Error: <%= error.message %>"))
        .pipe(rename({
            extname: ".min.js"
         }))
        .pipe(gulp.dest('build'))
        .pipe(notify('Uglified JavaScript (' + moment().format('MMM Do h:mm:ss A') + ')'));
});

gulp.task('watch', function() {
    watch({
        glob: 'src/**/*.js'
    }, function() {
        gulp.start('uglify-js');
    });
});

gulp.task('build', function() {
    gulp.start('uglify-js');
});

gulp.task('default', ['watch']);
