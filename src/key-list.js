import Type from './type-checker';

const truthy = () => true;
const falsey = () => false;

export default class KeyList {
  constructor (list) {
    this.extend(list);
  }

  extend (list) {
    list
    .map((target) => Type.isArray(target) ? target : [target, truthy])
    .forEach((target) => this[target[0]] = target[1]);
  }

  remove (list) {
    if (!Type.isArray(list)) list = [list];
    list.forEach((key) => delete this.key);
  }

  check (key, ...args) {
    return (key in this) ? this[key](args) : falsey();
  }
}
