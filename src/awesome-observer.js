import ProxyHandlerFactory from './proxy-handler-factory';
import Observer from './observer';
import privatize from './privatizer';

export default {
  observe (target, config = {}) {
    const factory = new ProxyHandlerFactory(config);
    return Observer.observe({
      target,
      proxyHandler: factory.handler,
      parent: null,
      root: target
    });
  },

  getParent: Observer.getParent,

  getRoot: Observer.getRoot,

  getUid: Observer.getUid,

  isObserved: Observer.isObserved,

  isRoot: Observer.isRoot
};
