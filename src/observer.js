import Exception from './exception';
import Type from './type-checker';
import privatize from './privatizer';

const defineGetters = (target, ...args) => {
  args
    .map((arg) => ({key: arg[0], value: arg[1]}))
    .forEach(
      ({key, value}) => Object.defineProperty(target, key, { get: () => value })
    );
};

const Observer = {
  observe ({target, proxyHandler, parent, root}) {
    if (this.isObservableObject(target)) return this.observeObject({target, proxyHandler, parent, root});
    else if (this.isObservableArray(target)) return this.observeArray({target, proxyHandler, parent, root});
    else return target;
  },

  observeObject ({target, proxyHandler, parent, root}) {
    Object.keys(target).forEach((key) => target[key] = this.observe({target: target[key], parent: target, proxyHandler, root}));
    return this.createProxy({target, proxyHandler, parent, root});
  },

  observeArray ({target, proxyHandler, parent, root}) {
    target.forEach((value, key) => target[key] = this.observe({target: value, parent: target, proxyHandler, root}));
    return this.createProxy({target, proxyHandler, parent, root});
  },

	createProxy ({target, proxyHandler, parent, root}) {
    defineGetters(target,
      ["@isObserved", true],
      ["@parent", parent],
      ["@root", root],
      ["@isRoot", target === root],
      ["@proxyHandler", target === root ? proxyHandler : null],
      ["@uid", Math.random().toString(36).substr(2, 10)]
    );

		return new Proxy(target, proxyHandler);
	},

  extend ({target, key, value}) {
    const root = this.isRoot(target) ? target : this.getRoot(target);
    target[key] = this.observe({
      target: value,
      proxyHandler: privatize(root).proxyHandler,
      parent: target,
      root
    });
  },

  isObservableObject (target) {
    return Type.isObject(target) && !this.isObserved(target);
  },

  isObservableArray (target) {
    return Type.isEnumerable(target) && !this.isObserved(target);
  },

  getParent (target) {
    return privatize(target).parent;
  },

  getRoot (target) {
    return privatize(target).root;
  },

  getUid (target) {
    return privatize(target).uid;
  },

  isObserved (target) {
    return !!privatize(target).isObserved;
  },

  isRoot (target) {
    return !!privatize(target).isRoot;
  }
};

export default Observer;
