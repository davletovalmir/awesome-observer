const handler = {
  get (target, key) {
    return target[`@${key}`];
  },
  set (target, key, value) {
    target[`@${key}`] = value;
    return true;
  }
};

const privatize = (target) => new Proxy(target, handler);

export { privatize as default };
