import SubscriptionManager from './subscription-manager';
import Observer from './observer';
import Type from './type-checker';
import KeyList from './key-list';
import Exception from './exception';

const skipList = new KeyList([
  'subscribe',
  '@isObserved',
  '@parent',
  '@root',
  '@isRoot',
  '@proxyHandler',
  '@uid',
  ['length', (target) => Type.isEnumerable(target)]
]);

const handlers = {
  getter (target, key, proxy) {
    const value = target[key];
    this.sm.execute("get", {target, key, value});
    return value;
  },
  setter: {
    allowNewValues (target, key, value, proxy) {
      if (skipList.check(key, target)) return;

      const prevValue = target[key];
      if (prevValue === value) return;

      if (Type.isUndefined(prevValue)) this.sm.execute('new', {target, key, value});
      else this.sm.execute('change', {target, key, value, prevValue});

      Observer.extend({target, key, value});
    },
    disallowNewValues (target, key, value, proxy) {
      if (skipList.check(key, target)) return;

      const prevValue = target[key];
      if (prevValue === value) return;

      if (Type.isUndefined(prevValue)) return new Exception('New values are not allowed');

      this.sm.execute('change', {target, key, value, prevValue});
      Observer.extend({target, key, value});
    }
  },
  remover: {
    allowDeletingValues (target, key) {
      const prevValue = target[key];
      if (Type.isUndefined(prevValue)) return;

      if (skipList.check(key, target)) return;

      delete target[key];
      this.sm.execute('delete', {target, key, prevValue});
    },
    disallowDeletingValues (target, key) {
      return new Exception('Deleting values is not allowed');
    }
  }
};

export default class ProxyHandlerFactory {
  constructor (config) {
    this.config = Object.assign({}, this.defaultConfig, config);
    this.sm = new SubscriptionManager();
  }

  get handler () {
    const self = this;

    return {
      get (target, key, proxy) {
        if (key === 'subscribe') return self.sm.buildSubscribeFor(target);
        if (key === Symbol.toPrimitive) return () => `[Observed, uid: ${AwesomeObserver.getUid(target)}]`;
        return self.getter(target, key, proxy);
      },
      set (target, key, value, proxy) {
        self.setter(target, key, value);
        return true;
      },
      deleteProperty (target, key) {
        self.remover(target, key);
        return true;
      }
    };
  }

  get defaultConfig () {
    return {
      allowNewValues: true,
      allowDeletingValues: true
    };
  }

  get getter () {
    return handlers.getter.bind(this);
  }

  get setter () {
    if (this.config.allowNewValues) return handlers.setter.allowNewValues.bind(this);
    else return handlers.setter.disallowNewValues.bind(this);
  }

  get remover () {
    if (this.config.allowDeletingValues) return handlers.remover.allowDeletingValues.bind(this);
    else return handlers.remover.disallowDeletingValues.bind(this);
  }
}
