export default {
  isEnumerable (target) {
    return typeof target === 'object' && typeof target[Symbol.iterator] === 'function';
  },

  isArray (target) {
    return Array.isArray(target);
  },

  isObject (target) {
    return typeof target === 'object' && target !== null;
  },

  isNull (target) {
    return target === null;
  },

  isUndefined (target) {
    return target === undefined;
  },

  hasValue (target) {
    return !this.isNull(target) && !this.isUndefined(target);
  }
};
