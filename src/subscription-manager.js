import Observer from './observer';
import Subscription from './subscription';
import Exception from './exception';
import Type from './type-checker';
import privatize from './privatizer';

let __ = null;

export default class SubscriptionManager {
  constructor () {
    __ = privatize(this);
    __.subscribers = {};
  }

  execute (eventType, {target, key, value, prevValue}) {
    const targetsChain = [];
    let current = target;
    while (Type.hasValue(current)) {
      targetsChain.push(current);
      current = Observer.getParent(current);
    }

    targetsChain.forEach((model) => {
      const uid = Observer.getUid(model),
            subscribers = __.subscribers[uid];

      if (!subscribers) return;
      const subscriptions = subscribers[eventType];

      subscriptions.forEach((subscription) => {
        if (__.matchesToTarget({subscription, model, key})) {
          subscription.handler({
            key,
            value,
            prevValue,
            target,
            currentTarget: model,
            parent: Observer.getParent(model),
            root: Observer.getRoot(model)
          });
        }
      });
    });
  }

  buildSubscribeFor (target) {
    return __.subscribe.bind(this, target);
  }

  "@getOrBuildSubscriptions" (uid) {
    const subscriptions = __.subscribers[uid];

    if (Type.hasValue(subscriptions)) return subscriptions;

    return {
      'change': [],
      'new': [],
      'delete': [],
      'get': []
    }
  }

  "@subscribe" (target, params) {
    __.add({
      target,
      eventType: params.on,
      handler: params.do,
      fields: params.only
    });
  }

  "@add" ({eventType, target, handler, fields}) {
    const uid = Observer.getUid(target);
    const subscriptions = __.getOrBuildSubscriptions(uid);

    if (!(eventType in subscriptions)) return new Exception(`You can't subscribe to unexisting "${eventType}" event.`);
    if (typeof fields === 'function') [fields, handler] = [undefined, fields];

    const subscription = new Subscription({eventType, handler, target, fields});


    subscriptions[eventType].push(subscription);
    __.subscribers[uid] = subscriptions;

    return subscription;
  }

	"@matchesToTarget"({subscription, target, key}) {
		if (subscription.fields) return subscription.fields.indexOf(key) !== -1;
		else return true;
	}
}
