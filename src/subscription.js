export default class Subscription {
	constructor({eventType, target, fields, handler}) {
		this.eventType = eventType;
		this.target = target;
		this.fields = fields;
		this.handler = handler;
	}

	updateFields(fields) {
		try {
			if (Array.isArray(fields)) this.fields = fields;
			else if (typeof fields === 'function') fields.call(this, this.fields);
			else throw new Error('#fields are only allowed as array or function');
			return true;
		} catch (e) {
			return false;
		}
	}

	updateHandler(handler) {
		try {
			if (typeof handler === 'function') this.handler = handler;
			else throw new Error('#handler is only allowed as function');
			return true;
		} catch (e) {
			return false;
		}
	}
}
